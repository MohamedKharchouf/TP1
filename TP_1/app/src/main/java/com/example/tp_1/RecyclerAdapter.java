package com.example.tp_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp_1.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    // créationdesdonnées statiques
        private String[] titles = { "France",
        "Allemagne",
        "Espagne",
        "Afrique du Sud",
        "Etats-Unis",
        "Japan"};
        private String[] details = {"Paris",
        "Berlin",
        "Madrid",
        "Le Cap",
                "Washington DC",
                "Tokyo"
        };
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.image);
            itemTitle = itemView.findViewById(R.id.Pays);
            itemDetail = itemView.findViewById(R.id.Capital);

            int position = getAdapterPosition();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
            public void onClick(View v) {
                    int position = getAdapterPosition();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action =
                            ListFragmentDirections.actionFirstFragmentToSecondFragment(position);
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }});
        }
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup,int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup,false);
        ViewHolder viewHolder =new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,int i) {
        viewHolder.itemTitle.setText(titles[i]);
        viewHolder.itemDetail.setText(details[i]);
        String uri = Country.countries[i].getImgUri();
        Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri,null,c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
