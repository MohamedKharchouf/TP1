package com.example.tp_1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tp_1.data.Country;

public class DetailFragment extends Fragment {
    TextView itemPays;
    ImageView itemFlag;
    EditText itemCap;
    EditText itemLangues;
    EditText itemMonnaie;
    EditText itemPopulation;
    EditText itemSuperficie;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        itemPays = view.findViewById(R.id.pays2);
        itemFlag = view.findViewById(R.id.image2);
        itemCap = view.findViewById(R.id.detail);
        itemLangues = view.findViewById(R.id.detail2);
        itemMonnaie = view.findViewById(R.id.detail3);
        itemPopulation = view.findViewById(R.id.detail4);
        itemSuperficie = view.findViewById(R.id.detail5);
        String uri = Country.countries[args.getCountryId()].getImgUri();
        Context c = itemFlag.getContext();
        itemFlag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri,null,c.getPackageName())));
        itemPays.setText(Country.countries[args.getCountryId()].getName());
        itemCap.setText(Country.countries[args.getCountryId()].getCapital());
        itemMonnaie.setText(Country.countries[args.getCountryId()].getCurrency());
        itemSuperficie.setText(Integer.toString(Country.countries[args.getCountryId()].getArea())+" km2");
        itemLangues.setText(Country.countries[args.getCountryId()].getLanguage());
        itemPopulation.setText(Integer.toString(Country.countries[args.getCountryId()].getPopulation()));
        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}